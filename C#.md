using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        IWebDriver driver = new ChromeDriver();


        static void Main(string[] args)
        {   
        }

        
        [SetUp]

        public void Initialize()
        {
            driver.Navigate().GoToUrl("https://danibudi.github.io/Cross-Site%20Scripting%20(XSS).html");
            Console.WriteLine("Opened URL");
        }


        [Test]
        public void ExecuteTest()
        {
            IWebElement element = driver.FindElement(By.Name("q"));

            element.SendKeys("executeautomation");

            Console.WriteLine("Executed Test");
        }

        [Test]
        public void NextTest()
        {
            Console.WriteLine("Next Method");
        }

        [TearDown]

        public void CleanUp()
        {
            driver.Close();
        }

    }

   
}